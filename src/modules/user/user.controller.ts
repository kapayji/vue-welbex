import { Controller, Get, Param, Response, UseGuards } from '@nestjs/common';
import { UserGuard } from './user.guard';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('notes')
  getAllNotes(@Response() res) {
    return this.userService.getAllNotes(res);
  }

  @Get('notes/:email')
  @UseGuards(UserGuard)
  getUserNotes(@Param('email') param, @Response() res) {
    return this.userService.getUserNotes(param, res);
  }
  @Get(':email')
  @UseGuards(UserGuard)
  getUserOnRefresh() {
    return this.userService.getUserOnRefresh();
  }
}
