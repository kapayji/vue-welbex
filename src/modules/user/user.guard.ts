import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
@Injectable()
export class UserGuard implements CanActivate {
  constructor(private readonly jwtService: JwtService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    try {
      const req: Request = context.switchToHttp().getRequest();
      const token = (req.headers.authorization || '').replace('Bearer ', '');
      const decode = this.jwtService.verify(token, {
        secret: process.env.CLIENT_SECRET,
      });
      const { email } = req.params;
      return email === decode.email ?? false;
    } catch (e) {
      return false;
    }
  }
}
