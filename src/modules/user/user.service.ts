import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from 'src/entities/user.entity';
import { FindOneOptions, Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(Users)
    private readonly usersRepository: Repository<Users>,
  ) {}
  async getAllNotes(res): Promise<any[] | string> {
    const allUsersWithNotes = await this.usersRepository.find({
      relations: ['usersId', 'usersId.noteId'],
    });
    const resultUser = allUsersWithNotes
      .filter(({ usersId }) => usersId.length)
      .map(({ email, userName, usersId }) => ({
        email,
        userName,
        usersId,
      }));
    return res.json(resultUser);
  }
  async getUserNotes(user: string, res): Promise<Users | string> {
    try {
      const config: FindOneOptions<Users> = {
        relations: ['usersId', 'usersId.noteId'],
      };
      if (user) {
        config.where = { email: user };
      }
      const { email, userName, usersId } = await this.usersRepository.findOne(
        config,
      );
      return res.json({ email, userName, usersId });
    } catch (e) {
      console.log(e);
      return res.json(
        await this.usersRepository.findOne({ where: { email: user } }),
      );
    }
  }
  async getUserOnRefresh(): Promise<boolean> {
    return true;
  }
  async findOne(email: string): Promise<Users> {
    return await this.usersRepository.findOne({ email });
  }
}
