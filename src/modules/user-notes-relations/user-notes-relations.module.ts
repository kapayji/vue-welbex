import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users_Notes } from 'src/entities/users-notes.entity';
import { UserModule } from '../user/user.module';
import { UserNotesRelationsService } from './user-notes-relations.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Users_Notes]),
    forwardRef(() => UserModule),
  ],
  providers: [UserNotesRelationsService],
  exports: [UserNotesRelationsService],
})
export class UserNotesRelationsModule {}
