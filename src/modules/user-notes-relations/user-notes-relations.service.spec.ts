import { Test, TestingModule } from '@nestjs/testing';
import { UserNotesRelationsService } from './user-notes-relations.service';

describe('UserNotesRelationsService', () => {
  let service: UserNotesRelationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserNotesRelationsService],
    }).compile();

    service = module.get<UserNotesRelationsService>(UserNotesRelationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
