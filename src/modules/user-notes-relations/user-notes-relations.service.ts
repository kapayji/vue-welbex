import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Users_Notes } from 'src/entities/users-notes.entity';
import { Repository } from 'typeorm';
import { UserService } from '../user/user.service';

@Injectable()
export class UserNotesRelationsService {
  constructor(
    @InjectRepository(Users_Notes)
    private readonly usersNotes: Repository<Users_Notes>,
    private readonly userService: UserService,
  ) {}
  async setRelation(email: string, noteId: number): Promise<void> {
    const user = await this.userService.findOne(email);
    const newRelation = {
      userId: user.id,
      noteId,
    };
    const aaa = await this.usersNotes.save(newRelation);
    console.log(aaa);
  }
  // async deleteRelation(){}
}
