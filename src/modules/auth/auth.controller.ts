import { Body, Controller, Post, Response } from '@nestjs/common';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('registration')
  async registrationUser(@Body() body, @Response() res) {
    console.log(body);
    return this.authService.registrationUser(body, res);
  }
  @Post('login')
  async loginUser(@Body() body, @Response() res) {
    console.log(body);
    return this.authService.loginUser(body, res);
  }
}
