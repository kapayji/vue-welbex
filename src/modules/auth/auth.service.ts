import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from 'src/entities/user.entity';
import { User } from 'src/interfaces/user.interface';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { Res } from 'src/interfaces/res.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    @InjectRepository(Users)
    private readonly usersRepository: Repository<Users>,
  ) {}
  async registrationUser(user: User, res): Promise<Users | Res> {
    const isExist = await this.usersRepository.findOne({ email: user.email });
    console.log(user.email);
    if (isExist) {
      const resMessage: Res = { message: 'Already exist' };
      return res.status(400).json(resMessage);
    }
    // const token = this.jwtService.sign(
    //   { email: user.email },
    //   { secret: process.env.CLIENT_SECRET },
    // );
    // user.token = token;
    // console.log(token);
    const hashedPassword = await bcrypt.hash(user.password, 12);
    user.password = hashedPassword;
    const newUser: Users = await this.usersRepository.save(user);
    return res.json(newUser);
  }
  async loginUser(user: User, res): Promise<Users | string> {
    const { email, password } = user;
    const founded = await this.usersRepository.findOne({
      where: { email },
    });
    console.log(founded);
    if (!founded) {
      const resMessage: Res = { message: 'Not found' };
      return res.status(400).json(resMessage);
    }
    const passwordIsMatch = await bcrypt.compare(password, founded.password);
    console.log(password);
    if (!passwordIsMatch) {
      const resMessage: Res = { message: 'Неверный пароль' };
      return res.status(400).json(resMessage);
    }
    const token = this.jwtService.sign(
      { email: user.email },
      { secret: process.env.CLIENT_SECRET },
    );
    founded.token = token;
    await this.usersRepository.save(founded);
    return res.json({
      email: founded.email,
      token: founded.token,
      userName: founded.userName,
    });
  }
}
