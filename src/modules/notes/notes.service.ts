import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Notes } from 'src/entities/notes.entity';
import { Note } from 'src/interfaces/note.interface';
import { Repository } from 'typeorm';
import { UserNotesRelationsService } from '../user-notes-relations/user-notes-relations.service';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(Notes)
    private readonly notesRepository: Repository<Notes>,
    private readonly userNoteRelationService: UserNotesRelationsService,
  ) {}
  async notesCreateAndUpdate(
    body: {
      email: any;
      textMessage: any;
      creatingDate: any;
    },
    attachFile: { originalname: any; mimetype: any; buffer: any },
  ): Promise<void> {
    const { email, textMessage, creatingDate } = body;
    const note: Note = { textMessage, creatingDate };
    if (attachFile) {
      const { originalname, mimetype, buffer } = attachFile;
      note.attachFile = {
        originalname,
        mimetype,
        buffer: Buffer.from(buffer).toString('base64'),
      };
    }
    const newNote = await this.notesRepository.save(note);
    this.userNoteRelationService.setRelation(email, newNote.id);
  }
}
