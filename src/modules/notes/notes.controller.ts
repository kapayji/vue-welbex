import {
  Body,
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { NotesService } from './notes.service';

@Controller('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file', {}))
  async notesCreateAndUpdate(@Body() body, @UploadedFile() file: any) {
    try {
      return await this.notesService.notesCreateAndUpdate(body, file);
    } catch (e) {
      console.log(e);
    }
  }
}
