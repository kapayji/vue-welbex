import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Notes } from 'src/entities/notes.entity';
import { UserNotesRelationsModule } from '../user-notes-relations/user-notes-relations.module';
import { NotesController } from './notes.controller';
import { NotesService } from './notes.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Notes]),
    forwardRef(() => UserNotesRelationsModule),
  ],
  providers: [NotesService],
  controllers: [NotesController],
  exports: [NotesService],
})
export class NotesModule {}
