export interface User {
  userName: string;
  email: string;
  password?: string;
  token?: string;
  notesId?: any[];
}
