export interface Note {
  textMessage: string;
  creatingDate: string;
  attachFile?: {
    originalname: string;
    mimetype: string;
    buffer: string;
  };
  user_email?: string;
}
