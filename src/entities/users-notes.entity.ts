import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Notes } from './notes.entity';
import { Users } from './user.entity';

@Entity()
export class Users_Notes {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Users, (user) => user.id)
  @JoinColumn([{ name: 'userId' }, { name: 'id' }])
  userId: number;

  @ManyToOne(() => Notes, (note) => note.id)
  @JoinColumn([{ name: 'noteId' }, { name: 'id' }])
  noteId: number;
}
