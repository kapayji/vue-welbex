import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm/index';
import { Users_Notes } from './users-notes.entity';

@Entity()
export class Notes {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  textMessage: string;

  @Column()
  creatingDate: string;

  @Column('simple-json', { nullable: true })
  attachFile: { originalname: string; mimetype: string; buffer: string };

  @OneToMany(() => Users_Notes, (users_notes) => users_notes.noteId)
  notesId?: Users_Notes[];
}
