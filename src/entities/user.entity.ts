import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm/index';
import { Users_Notes } from './users-notes.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  token?: string;

  @OneToMany(() => Users_Notes, (users_notes) => users_notes.userId)
  usersId?: Users_Notes[];
}
