import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  async use(req: Request, res: Response, next: NextFunction) {
    if (req.method === 'OPTIONS') {
      next();
    }
    try {
      console.log('Token will be here');
      const token = (req.headers.authorization || '').replace('Bearer ', '');
      // console.log(token);
      // console.log(jwt.verify(token, process.env.CLIENT_SECRET));
      if (!token) {
        throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
      }
      const decoded: any = jwt.verify(token, process.env.CLIENT_SECRET);
      const { email } = decoded;
      req.body.email = email;
      next();
    } catch (e) {
      // console.log(e);
      throw new HttpException('Expired', HttpStatus.BAD_REQUEST);
    }
  }
}
